'use strict';

/* global Firebase */
App
    .factory('FireConnBase', ['$firebase', function ($firebase) {
        var url = 'https://the-board.firebaseio.com/',
            ref = new Firebase(url);

        return $firebase(ref);
    }])

    .factory('FireConnOptions', ['$firebase', function ($firebase) {
        var url = 'https://the-board.firebaseio.com/options',
            ref = new Firebase(url);

        return $firebase(ref);
    }])

    // This factory is looking for the db.json file and getting the
    // information from the file and returning it to the controller calling it
    .factory('dbFactory', function ($firebase) {
        return {
            // This will get the data from the DB
            getMenuData: function (type) {
                // creating a new instance of Firebase and assigning it to a var
                var options = new Firebase('https://the-board.firebaseio.com/options');
                // Returning agular-fire with the options passed in
                return $firebase(options);
            },

            // Returning the options within a menu
            getOptionData: function (option) {
                var data = new Firebase('https://the-board.firebaseio.com/options/'+option);
                return $firebase(data);
            },

            // Returns the individual event
            getEventData: function (option, event) {
                var data = new Firebase('https://the-board.firebaseio.com/options/' + option + '/events/' + event);
                return $firebase(data);
            },

            pushData: function () {

            }
        };
    });
