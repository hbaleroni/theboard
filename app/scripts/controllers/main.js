'use strict';

/* global Firebase */
App
    // Controller for the Main Menu for the app
    .controller('MenuCtrl', ['$scope', 'dbFactory', function ($scope, dbFactory) {
        // Using the factory to send the db info to view
        $scope.options = dbFactory.getMenuData($scope.eventType);
    }])

    // Used to get data about a specific activity
    .controller('OptionCtrl', ['$scope', 'dbFactory','$routeParams', function ($scope, dbFactory,$routeParams) {
        $scope.option = dbFactory.getOptionData($routeParams.pageId);
    }])

    // Used to get data about a specific event
    .controller('EventCtrl', ['$scope', 'dbFactory','$routeParams', '$rootScope', '$http', function ($scope, dbFactory, $routeParams, $rootScope, $http) {
        $scope.eventType = $routeParams.type;
        $scope.eventMenu = dbFactory.getEventData($scope.eventType, $routeParams.pageId);

        // setting FB id to be used in the attending function
        // This is breaking all the previous events that do not have the FB ID saved to Firebase
        // The only event that currently has the FB ID is Web Event 1... TO see the other events working
        // just comment out this line and they will populate properly

//        $rootScope.fbID = $scope.eventMenu.fbID.id; // <<--- this line is the breaker of codes

        // Function to set user to attend the FB event
        // This function is returning an OAuth error, probably about permissions
        // Will work on this if time permits
        $scope.attendEvent = function () {
            var str = 'https://graph.facebook.com/' + $rootScope.fbID + '/attending/' + '?callback=JSON_CALLBACK&access_token=' + $rootScope.user.accessToken;

            $http({method: 'post', url: str})
                .success(function(data, status) {
                    console.log('success', data, status);
                })
                .error(function(data, status) {
                    console.log('error', data, status);
                });
        }
    }])

    // Controller to create events
    .controller('NewCtrl', ['$scope', '$rootScope', 'FireConnOptions', '$http', '$routeParams', function ($scope, $rootScope, FireConnOptions, $http, $routeParams) {
        $scope.submitEvent = function () {
            // Setting up the format for the date for FB
            var date = $scope.newEvent.date.split('-'),
                time = $scope.newEvent.time.split(':');
            var dates = new Date(date[0], parseInt(date[1])-1, date[2], time[0], time[1]),
                fbDate = dates.toISOString();

            console.log(fbDate);

            // String used to create a FB event, values being input from form
            var str = 'https://graph.facebook.com/me/events?access_token='+ $rootScope.user.accessToken + '&name=' + $scope.newEvent.name + '&start_time=' + fbDate + '&description=' + $scope.newEvent.description + '&location=' + $scope.newEvent.location + '&privacy_type=OPEN';

            $http({method: 'post', url: str})
                .success(function (data, status) {
                    console.log('success', data, status);
                    console.log('ran success');

                    // Inserting FB Event ID into the obj
                    $scope.newEvent.fbID = data;
                    console.log($scope.newEvent.fbID);

                    // Inserting event to firebase
                    $scope.eventRef = FireConnOptions;

                    // Creating var to assign event to correct DB field
                    var type = $scope.newEvent.type;

                    // Adding the event to the DB
                    $scope.eventRef.$child(type + '/events').$add($scope.newEvent);

                    // Rerouting to home
                    location.href = '#/home';
                })
                .error(function (data, status) {
                    console.log('error', data, status);
                    console.log('ran error');
                });
        };

        // Variables used to pass data into the type field of the form
        // so, it is predefined based on the category
        $scope.eventType = $routeParams.type;
        $scope.newEvent = {type : $scope.eventType};
    }])

    // Controller used to login with FB
    .controller('LoginCtrl', ['$scope', '$rootScope', '$firebaseSimpleLogin',
        function($scope, $rootScope, $firebaseSimpleLogin) {
            var dataRef = new Firebase('https://the-board.firebaseio.com');
            $scope.loginObj = $firebaseSimpleLogin(dataRef);

            // Function called to run the login
            $scope.facebookLogin = function() {
                $scope.loginObj.$login('facebook', {
                    rememberMe : true,
                    scope: 'create_event, rsvp_event'
                },{
                    return_scopes: true
                }).then(function (user) {
                    console.log('Logged in as: ', user);
                    location.href = '#/home';
                    // Assigning the user obj to the rootScope for access throughout the app
                    $rootScope.user = user;
                }, function (error) {
                    console.error('Login failed: ', error);
                });
            };

            // Logout function and redirecting to the landing page
            $scope.logout = function() {
                $scope.loginObj.$logout();
                location.href = '/';
            };
        }
    ]);