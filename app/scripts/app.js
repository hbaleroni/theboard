'use strict';

/* global Firebase */
var App = angular.module('theBoardApp', [
    'ngRoute',
    'firebase',
    'ui.bootstrap'
]);

App.config(function ($routeProvider) {
$routeProvider
    .when('/', {
        templateUrl: 'views/landing.html',
        controller: 'LoginCtrl'
    })
    .when('/home', {
        templateUrl: 'views/main.html',
        controller: 'MenuCtrl'
    })
    .when('/menu/:pageId', {
        templateUrl: 'views/option.html',
        controller: 'OptionCtrl'
    })
    .when('/event/:type/:pageId', {
        templateUrl: 'views/event.html',
        controller: 'EventCtrl'
    })
    .when('/:type/new-event', {
        templateUrl: 'views/new-event.html',
        controller: 'NewCtrl'
    })
    .otherwise({
        redirectTo: '/'
    });
});

App.filter('toArray', function () {
    'use strict';

    return function (obj) {
        if (!(obj instanceof Object)) {
            return obj;
        }

        return Object.keys(obj).filter(function(key){if(key.charAt(0) !== "$") {return key;}}).map(function (key) {
            return Object.defineProperty(obj[key], '$key', {__proto__: null, value: key});
        });
    };
});